import Head from 'next/head';
// import Container from 'react-bootstrap/Container';
import Link from 'next/link';
import { getCookies, deleteCookie } from 'cookies-next';
import { useState, useEffect } from 'react'
import { LoginUseRoute } from '../lib/post';
import { useRouter } from 'next/router';
import { Nav, Navbar } from 'react-bootstrap';

const Layout = ({ children }: any) => {
    const [userIf, setUserIf] = useState([]);
    const [Search, setSearch] = useState<string>('');
    const router = useRouter();
    useEffect(() => {
        // if (checklegth.isReady) {
        getData();

    }, [])
    const getData = async () => {
        const getuserIf = await LoginUseRoute();
        setUserIf(getuserIf)
    }
    const checklegth = getCookies();

     
    const checkCookie = async ({ req, res }: any) => {
        await deleteCookie('user', { req, res });
        router.push("/")
    }
    
    const handleSearch = (event: any) => {
        event.preventDefault()
        router.replace(`/?q=${Search}`)
    }
    return (
        <div className="container">
            <Head>
                <meta charSet='UTF-8' />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>My Next App </title>
            </Head>
            <Navbar style={{ background: "#89e9e6" }} >
                <Navbar.Brand>Blog </Navbar.Brand>
                < Nav >
                    <Link href="/" passHref>
                        <Nav.Link>Home </Nav.Link>
                    </Link>
                    <Link href="/NewBlog/NewBlog" passHref>
                        <Nav.Link>New Blog </Nav.Link>
                    </Link>
                    <nav >
                        <div className="container-fluid">
                            <form className="d-flex" role="search">
                                <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"
                                    value={Search}
                                    onChange={(e) => setSearch(e.target.value)}
                                />
                                <button className="btn btn-outline-success " onClick={handleSearch}>Search</button>
                            </form>
                        </div>
                    </nav>

                    {checklegth.user && checklegth.user.length > 0 ?
                        <>
                            <Link href="/Wall" passHref>
                                <Nav.Link > Hello
                                    {userIf}
                                </Nav.Link>
                            </Link>
                            <Nav.Link
                                onClick={checkCookie}
                            >Đăng Xuất</Nav.Link>
                        </>
                        :
                        <Link href="/Accout/Login" passHref>
                            <Nav.Link >Đăng Nhập</Nav.Link>
                        </Link>
                    }
                </Nav>
                <div>

                </div>
            </Navbar>

            < main > {children} </main>
        </div >
    )
}

export default Layout

