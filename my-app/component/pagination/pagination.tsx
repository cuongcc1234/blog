import React from 'react';
import _ from 'lodash'
import { useEffect } from 'react'
interface Props {
    items: number,
    pageSize: number,
    currentPage: number,
    onPageChange: any
}

const Pagination = ({ items, pageSize, currentPage, onPageChange }: Props) => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    const pageCount = items / pageSize;
    if (Math.ceil(pageCount) === 1) return null;
    const pages = _.range(1, pageCount + 1);
    return (
        <>
            <nav>
                <ul className="pagination">
                    {pages.map((page) => (
                        <li
                            key={page}
                            className={
                                page === currentPage ? "page-item active" : "page-item"
                            }
                        >
                            <a
                                style={{ cursor: "pointer" }}
                                onClick={() => onPageChange(page)}
                                className="page-link"
                            >
                                {page}
                            </a>
                        </li>
                    ))}
                </ul>
            </nav>
        </>
    )
}

export default Pagination
