import axios from "axios";

export const postUser = async (body: object) => {
  return await axios.post("/api/user", body);
};
export const LoginUser = async (body: object) => {
  return await axios.post("/api/userLogin", body);
};
export const LoginUseRoute = async () => {
  try {
    const response = await axios.get("/api/userLogin");
    return response.data;
  } catch (e) {
    console.log(e);
  }
};

export const postBlog = async (body: object) => {
  return await axios.post("/api/NewBlog", body);
};

export const GetDataB = async () => {
  try {
    const response = await axios.get("/api/GetBlog");

    return response.data;
    // console.log(response.data.savedBlog);
  } catch (e) {
    console.log(e);
  }
};
export const GetDataInvi = async () => {
  try {
    const response = await axios.get("/api/getBlogIndi");
    return response.data;
    // console.log(response.data.savedBlog);
  } catch (e) {
    console.log(e);
  }
};

export const GetBlogInfor = async (id: string) => {
  try {
    const response = await axios.get(`/api/GetBlogId/${id}`);
    return response.data;
  } catch (e) {
    console.log(e);
  }
};

export const GetSearch = async (q: any) => {
  try {
    const response = await axios.get(`/api/GetBlogSearch?q=${q}`);
    return response.data;
  } catch (e) {
    console.log(e);
  }
};
export const DeletePost = async (Id: object) => {
  return await axios.post("/api/profile/Delete", Id);
};

export const UpdatePost = async (post: object) => {
  return await axios.post("/api/profile/UpdatePost", post);
};

export const View = async (post: any) => {
  return await axios.patch("/api/GetBlogId/[id]", post);
};
export const Like = async (id: object) => {
  return await axios.patch("/api/GetBlog", id);
};
export const SortLike = async (nameSort: object) => {
  return await axios.post("/api/GetBlog", nameSort);
};
