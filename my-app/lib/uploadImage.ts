import axios from "axios";

export const uploadFile = async (fileName: any) => {
  const formData = new FormData();
  formData.append("file", fileName[0]);
  formData.append("upload_preset", "ntwklzeh");
  const { data } = await axios.post(
    "https://api.cloudinary.com/v1_1/dff41zenf/image/upload",
    formData
  );
  console.log(data.url);

  return data.url;
};
