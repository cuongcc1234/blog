import React, { useState } from 'react'
import Layout from '../../component/Layout';
import { postBlog } from '../../lib/post'
import { getCookies } from 'cookies-next';
import { useRouter } from 'next/router';
import {uploadFile} from "../../lib/uploadImage";

const NewBlog = () => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState(' ');
    const [type, setType] = useState('Thế Giới');
    const checklegth = getCookies();
    const [image, setImage] = useState<any>();
    // const [imageUpdate,setImageUpdate]=useState("")
    
    const router = useRouter();

    const handleSend = async () => {
        if (!title || !content) {
            alert("viết vài chữ đi rồi đăng")
        }
        else {
            if (!(checklegth.user && checklegth.user.length > 0)) {
                alert("Đăng nhập đã bạn eii")
            }
            else {
                try {
                const url= await uploadFile(image);
                    await postBlog({ title, content, type,url });
                    router.push("/");
                } catch (error) {
                    // console.log("Error", error.response.data.message);
                }
            }
        }
    }
   
    return (
        <Layout>
            <div className='todo-list'>
                <h3>Thêm Blog</h3>
                <h5>Tiêu đề bài viết:</h5>
                <input style={{ width: "200px", height: "40px" }} type='text' placeholder='nhập tiêu đề' value={title}
                    onChange={(e) => setTitle(e.target.value)}
                />
                <br />
                <br />
                <label htmlFor="type">Thể Loại:</label>
                <select id="type" onChange={(event) => setType(event.target.value)} >
                    <option value="Thế Giới" selected >Thế Giới</option>
                    <option value="Xã Hội">Xã Hội</option>
                    <option value="Văn Hóa">Văn Hóa</option>
                    <option value="Kinh Tế" >Kinh Tế</option>
                    <option value="Giáo Dục">Giáo Dục</option>
                    <option value="Thể Thao">Thể Thao</option>
                    <option value="Giải trí">Giải trí</option>
                    <option value="Pháp Luật" >Pháp Luật</option>
                    <option value="Khoa Học">Khoa Học</option>
                    <option value="Công Nghệ" >Công Nghệ</option>
                </select> 
                <br />
                <br />

                <input type="file"  onChange={(event) => setImage(event.target.files)} />
                    {/* <img src={imageUpdate}/> */}
                <br />
                <br />
                <h5>Nội dung bài viêt:</h5>

                <textarea rows={10} cols={160}
                    onChange={(e) => setContent(e.target.value)}
                >
                </textarea>
                <br />
                <button style={{ borderStyle: "dotted" }} onClick={handleSend} >Đăng</button>
            </div>
        </Layout >
    )
}

export default NewBlog
