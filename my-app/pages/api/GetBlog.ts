import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../lib/Prisma";

import cookie from "cookie";
import jwt from "jsonwebtoken";
import { connect } from "react-redux";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { method } = req;
    const q = req.query.q;

    if (method === "GET") {
      const savedBlog = await prisma.post.findMany({
        orderBy: {
          id: "desc",
        },
      });
      res.status(200).json(savedBlog);
    } else if (method === "PATCH") {
      const tokenHeader = req.headers.cookie;
      let cookies = cookie.parse(tokenHeader || "");
      let decode: any = jwt.verify(cookies.user, "abc");
      let decodeId = decode.id;
      const id = req.body.id.ID;
      const item = await prisma.post.findFirst({
        where: {
          id: id,
        },
      });
      if (item) {
        let checkLike = await prisma.history.findFirst({
          where: {
            userId: decodeId,
            postId: item.id,
            checklike: true,
          },
        });
        if (!checkLike) {
          let check = await prisma.history.findMany({
            where: {
              userId: decodeId,
            },
          });
          if (!check) {
            await prisma.history.create({
              data: {
                postId: item.id,
                userId: decodeId,
                checklike: true,
              },
            });
          } else {
            await prisma.history.updateMany({
              where: {
                postId: item.id,
                userId: decodeId,
              },
              data: {
                checklike: true,
              },
            });
          }
          await prisma.history.create({
            data: {
              postId: item.id,
              userId: decodeId,
              checklike: true,
            },
          });

          const Like = await prisma.post.updateMany({
            where: {
              AND: [
                {
                  id: item.id,
                },
              ],
            },
            data: {
              likes: Number(item.likes) + 1,
            },
          });
          res.status(200).json("Like ");
        } else {
          await prisma.history.updateMany({
            where: {
              postId: item.id,
              userId: decodeId,
            },
            data: {
              checklike: false,
            },
          });
          let unlike = await prisma.history.findMany({
            where: {
              checklike: false,
            },
          });
          if (unlike) {
            await prisma.post.update({
              where: {
                id: item.id,
              },
              data: {
                likes: Number(item.likes) - 1,
              },
            });
            res.status(200).json("Like ");
          }
        }
      }
    } else if (method === "POST") {
      const nameSort = req.body.namesort;
      if (nameSort === "Thế Giới") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Xã Hội") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Văn Hóa") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Kinh Tế") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Giáo Dục") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Thể Thao") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Giải Trí") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Pháp Luật") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Khoa Học") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else if (nameSort === "Công Nghệ") {
        const sortLike = await prisma.post.findMany({
          where: {
            type: nameSort,
          },
        });
        res.status(200).json(sortLike);
      } else {
        const savedBlog = await prisma.post.findMany({
          orderBy: {
            id: "desc",
          },
        });
        res.status(200).json(savedBlog);
      }
    }
  } catch (error) {
    res.status(400).json({
      errorcode: 0,
      message: "thất bại rồi",
    });
  }
};
