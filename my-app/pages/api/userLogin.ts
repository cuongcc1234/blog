import { NextApiRequest, NextApiResponse } from "next";
import bcrypt from "bcrypt";
import prisma from "../../lib/Prisma";
import { setCookie } from "cookies-next";
import jwt from "jsonwebtoken";
import cookie from "cookie";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { method } = req;
    if (method === "POST") {
      const { email, password } = req.body;
      const savedUser = await prisma.user.findFirst({
        where: {
          email: email,
          // password: password,
        },
      });
      if (!savedUser) {
        res.status(400).json({ message: "gmail chưa được đăng kí nhé bae!! " });
      }
      //dich pass
      const check = await bcrypt.compare(password, `${savedUser?.password}`);
      if (savedUser && check === false) {
        res.status(400).json({ message: "sai mật khẩu rồi cưng ơi!!" });
      }
      // token
      if (savedUser) {
        const accessTooken = jwt.sign(savedUser, "abc", { expiresIn: "1h" });
        setCookie("user", accessTooken, { req, res, maxAge: 60 * 60 });
        res.setHeader("user", accessTooken);

        res.status(200).json("đăng nhập thành công");
      }
    }
    if (method === "GET") {
      const cookies = cookie.parse(req.headers.cookie || "");
      // console.log('cookie', cookies);
      let dich: any = jwt.verify(cookies.user, "abc");
      // console.log(dich);
      res.status(200).json(dich.name);
    }
  } catch (error) {
    res.status(400).json({ message: "thất bại rồi đại vương ơi" });
  }
};
