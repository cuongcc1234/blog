import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../lib/Prisma";
import cookie from 'cookie';
import jwt from 'jsonwebtoken';
import { orderBy } from "lodash";
export default async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const tokenHeader = req.headers.cookie;
        let cookies = cookie.parse(tokenHeader || '');
        let dich: any = jwt.verify(cookies.user, 'abc');
        let id = dich.id;
        const GetBlogIndi = await prisma.post.findMany({
            where: {
                authorId: id
            },
            orderBy: {
                id: "desc"
            }
        });
        res.status(200).json(GetBlogIndi);

    } catch (error) {
        res.status(400).json({
            errorcode: 0,
            message: 'thất bại rồi'
        })
    }
}