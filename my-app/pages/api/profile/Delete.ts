import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/Prisma";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { id } = req.body;

    // await prisma.history.delete({
    //     where: {
    //         postId: id
    //     }
    // })
    const deletePost = await prisma.post.delete({
      where: {
        id: id,
      },
    });

    res.status(200).json("deletePost");
  } catch (error) {
    res.status(400).json({
      errorcode: 0,
      message: "thất bại rồi",
    });
  }
};
