import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/Prisma";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { id, title, content } = req.body;
    const deletePost = await prisma.post.update({
      where: {
        id: id,
      },
      data: {
        content: content,
        title: title,
      },
    });
    res.status(200).json("sua roi nhe!hihi");
  } catch (error) {
    res.status(400).json({
      errorcode: 0,
      message: "thất bại rồi",
    });
  }
};
