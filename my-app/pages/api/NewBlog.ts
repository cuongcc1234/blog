import { NextApiRequest, NextApiResponse } from "next";
import bcrypt from "bcrypt";
import prisma from "../../lib/Prisma";
import cookie from "cookie";
import jwt from "jsonwebtoken";
export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    // console.log(req.body);
    const tokenHeader = req.headers.cookie;
    let cookies = cookie.parse(tokenHeader || "");
    let decode: any = jwt.verify(cookies.user, "abc");
    const Iddecode = decode.id;
    const { title, content, type, url } = req.body;

    const NewBlog = await prisma.post.create({
      data: {
        title: title,
        content: content,
        type: type,
        authorId: Iddecode,
        image: url,
      },
    });
    res.status(200).json("Đăng bài thành công");
  } catch (error) {
    res.status(400).json({
      errorcode: 0,
      message: "thất bại rồi",
    });
  }
};
