import { NextApiRequest, NextApiResponse } from "next"
import bcrypt from 'bcrypt';
import prisma from "../../lib/Prisma";

export default async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        // console.log(req.body);
        const { name, email, password } = req.body;
        // const user: Prisma.UserCreateInput = JSON.parse(req.body);

        const exist = await prisma.user.findFirst({
            where: {
                email: email,
            }
        });
        if (exist) {
            return res.status(400).json({
                errorcode: 1,
                message: 'email đã tồn tại'
            })
        }
        const salt = bcrypt.genSaltSync(10);
        const newpass = await bcrypt.hash(password, salt);
        // console.log(newpass);

        const savedUser = await prisma.user.create({
            data: {
                name: name,
                password: newpass,
                email: email,

            }
        });
        res.status(200).json('thành công rồi đại vương ơi');
    } catch (error) {
        res.status(400).json({
            errorcode: 0,
            message: 'thất bại rồi'
        })
    }
}