import { Prisma } from "@prisma/client";
import { NextApiRequest, NextApiResponse } from "next"
import prisma from "../../lib/Prisma";

export default async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const q = req.query.q;
        const Post = await prisma.post.findMany({
            where: {
                OR: [
                    {
                        title: {
                            contains: String(q),
                            mode: "insensitive",
                        },
                    },
                    {
                        content: {
                            contains: String(q),
                            mode: "insensitive",

                        },
                    },
                ],
            }
        });
        res.status(200).json(Post);
    } catch (error) {
        res.status(400).json({
            errorcode: 0,
            message: 'thất bại rồi'
        })
    }
}