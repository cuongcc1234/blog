import { NextApiRequest, NextApiResponse } from "next";
import cookie from "cookie";
import jwt from "jsonwebtoken";
import prisma from "../../../lib/Prisma";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const tokenHeader = req.headers.cookie;
    let cookies = cookie.parse(tokenHeader || "");
    let decode: any = jwt.verify(cookies.user, "abc");
    const Iddecode = decode.id;

    const { method } = req;
    if (method === "GET") {
      const ID: any = req.query.id;
      if (ID) {
        const BlogId = await prisma.post.findFirst({
          where: {
            id: ID,
          },
        });
        const Clike = await prisma.history.findFirst({
          where: {
            postId: ID,
            userId: Iddecode,
          },
        });
        res.status(200).json({ BlogId, Clike });
      } else if (ID && tokenHeader) {
      }
    } else if (method === "PATCH") {
      const { id, views } = req.body;
      const postUpdate = await prisma.post.updateMany({
        where: {
          AND: [
            {
              id: id,
            },
          ],
        },
        data: {
          views: Number(views) + 1,
        },
      });
    }
  } catch (error) {
    res.status(400).json({
      errorcode: 0,
      message: "thất bại rồi",
    });
  }
};
