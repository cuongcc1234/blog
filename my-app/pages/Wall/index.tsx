
import Layout from '../../component/Layout'
import React, { useState, useEffect } from 'react';
import { GetDataInvi, DeletePost, UpdatePost } from '../../lib/post';
import { useRouter } from 'next/router';
interface Props {
    post: any,
    title: string,
    content: string
}
const Wall = (props: Props) => {
    const [post, setPost] = useState([]);
    const [check, setCheck] = useState(true);
    const [title, setTitle] = useState<string>('');
    const [content, setContent] = useState<string>('');
    const [id, setId] = useState<string>('');
    const router = useRouter();

    useEffect(() => {   
        getData();
    }, [])
    const getData = async () => {
        const getDataBlog = await GetDataInvi();
        setPost(getDataBlog);
    }

    const handleDelete = async (Id: object) => {
        await DeletePost({ id: Id });
        const getDataBlog = await GetDataInvi();
        setPost(getDataBlog);

    }

    const handleEdit = (post: any) => {
        setTitle(post.title);
        setContent(post.content);
        setId(post.id);
        setCheck(!check)
        
    }

    const handleSave = async () => {
        try {
            await UpdatePost({ title, content, id });
            setCheck(!check);
            const getDataBlog = await GetDataInvi();
         setPost(getDataBlog);
            

        } catch (error) {
            // console.log("Error", error.response.data.message);
        }

    }

    return (
        <Layout>

            {check && post.map((item: any) => {
                return (
                    <>
                        <div className=" card " key={item.id} >
                            <div className="card-body ">
                                <h5 className="card-title text-center">{item.title}</h5>
                                <p className="card-text text-truncate  ">{item.content}</p>

                            </div>

                            <div className="container px-4 text-center">
                                <div className="row gx-5">
                                    <div className="col">
                                        <button className="p-3 border bg-light" onClick={() => handleEdit({ id: item.id, content: item.content, title: item.title })}  >EDIT</button>
                                    </div>
                                    <div className="col">
                                        <button className="p-3 border bg-light" onClick={() => handleDelete(item.id)} >DELETE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                )
            })}
            {!check && <>

                <div className='todo-list'>
                    <h3>Sửa Blog</h3>
                    <h5>Tiêu đề bài viết:</h5>
                    <input style={{ width: "200px", height: "40px" }} type='text' placeholder='nhập tiêu đề'
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                    <br />
                    <br />
                    <h5>Nội dung bài viêt:</h5>
                    <textarea rows={8} cols={173}
                        value={content}
                        onChange={(e) => setContent(e.target.value)}
                    >
                    </textarea>
                    <br />
                    <button style={{ borderStyle: "dotted" }} onClick={handleSave}  >Save</button>
                </div>
            </>}
        </Layout>
    )
}

export default Wall
