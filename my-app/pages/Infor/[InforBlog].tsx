import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import Layout from '../../component/Layout';
import { GetBlogInfor, View, Like } from "../../lib/post";
import { getCookies } from 'cookies-next';
import {
    FacebookShareButton,
    FacebookIcon,
} from 'next-share';

interface Props {
    title: string,
    content: string
}
const InforBlog = (props: Props) => {
    const router = useRouter()
    const ID = router.query.InforBlog;
    const [post, setPost] = useState<any>({});
    const[checkLike,setCheckLike]=useState<boolean>(false)

    useEffect(() => {
        if (router.isReady) {
            getData(String(ID));
            handleLike
        }
    },[])

    const getData = async (id: string) => {
        const res = await GetBlogInfor(id);
        View(res.BlogId);
        setPost(res.BlogId);
        if(res.Clike){
            setCheckLike(res.Clike.checklike)
        }
  
    }
    const checklegth = getCookies();
    const handleLike = async (id: object) => {
        // if (!(checklegth.user && checklegth.user.length > 0)) {
        //     alert("Đăng nhập đã bạn eii")
        // } else {
            try {
                await Like({ id });
                setCheckLike(!checkLike)
            } catch (error) {
                console.log(error);
            }
    
    }
  
    
    return (
        <>
            <Layout>
                <div className=" card " >
                    <div className="card-body ">
                        <h3 className="card-title text-center">{post.title}</h3>
                        <div style={{textAlign:"center"}}>  <img src={`${post.image}`} className="img-fluid rounded-start" alt={post.title}  /></div>
                        <p className="card-text ">{post.content}</p>

                    </div>

                    <div className="container px-4 text-center">
                        <div className="row gx-12">
                            <div className="col">
                                <div className="p-3 border bg-light" style={{ cursor: "pointer" }}  onClick={() => handleLike({ ID })}>
                                    <button style={{ border: "none" }}>
                                        {checkLike==false?"like":"unlike"}
                                    </button>
                                </div>

                            </div>
                            <div className="col">
                              
                            </div>
                            <div className="col">
                                <div className="p-3 border bg-light">
                                    <FacebookShareButton
                                        url={'https://translate.google.com/?hl=vi'}
                                        quote={'next-share is a social share buttons for your next React apps.'}
                                        hashtag={'#myBlog'}
                                    >
                                        <FacebookIcon size={32} round />
                                    </FacebookShareButton>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </Layout>

        </>
    )
}

export default InforBlog
