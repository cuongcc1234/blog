import React, { useState, useEffect } from 'react';
import Layout from "../component/Layout"
import Link from 'next/link';
import Pagination from '../component/pagination/pagination';
import { paginate } from '../lib/Pagination';
import { useRouter } from 'next/router';
import { GetDataB, GetSearch, Like, SortLike } from '../lib/post';
import { getCookies } from 'cookies-next';

const Index = () => {
  const [Post, setPost] = useState<object[]>([]);
  const [CurrentPage, setCurrentPage] = useState<number>(1);
  const pageSize = 8;
  const Router = useRouter()
  let q = Router.query.q;
  const checklegth = getCookies();

  const getPost = async (q: any) => {
    const GetData = await GetDataB();
    if (q) {
      const GetDataSearch = await GetSearch(q);
      setPost(GetDataSearch)
    }
    if (!q) {
      return setPost(GetData)
    }
  }

  useEffect(() => {
    getPost(q);
    handleSort
  }, [q])

  const handlePageChange = (page: any) => {
    setCurrentPage(page)
  }
  const paginationPost = paginate(Post, CurrentPage, pageSize);
  const handleSort = async (post: string) => {
    try {
      const res = await SortLike({ 'namesort': post })
      setPost(res.data)
    } catch (error) {
      console.log(error);
    }
  }
const handleToggle=()=>{
  alert("dang nhap vao de!!!");
}
  return (
    <Layout  >
      <div className="menu container"  >
        <div className="btn-group" role="group" aria-label="Basic example">
          <button type="button" className="btn btn-primary" onClick={() => handleSort("")}>Tất Cả</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Thế Giới")}>Thế Giới</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Xã Hội")}>Xã Hội</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Văn Hóa")}>Văn Hóa</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Kinh Tế")}>Kinh Tế</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Giáo Dục")}>Giáo Dục</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Thể Thao")}>Thể Thao</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Giải Trí")}>Giải Trí</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Pháp Luật")}>Pháp Luật</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Khoa Học")}>Khoa Học</button>
          <button type="button" className="btn btn-primary" onClick={() => handleSort("Công Nghệ")}>Công Nghệ</button>
        </div>
      </div>

      <div className="row row-cols-1 row-cols-md-2 " >
        {paginationPost && paginationPost.map((item: any) => {
          return (
            <div className="col mb-6" key={item.id}>
              <div className="row g-0 p-2" >
                <div className="col-md-4">
                  <img src={`${item.image}`} className="img-fluid rounded-start" alt="..." style={{ height: "130px", width: "150px" }} />
                </div>
                <div className="col-md-8">
                  <div className="card-body" style={{ cursor: "pointer" }}>
                  {(checklegth.user && checklegth.user.length > 0)
                  ?
                  <> <Link href={`/Infor/${item.id}`} >
                      <h5 className="card-title">{item.title}</h5>
                    </Link>
                    </>
                    : 
                      <h5 className="card-title" onClick={handleToggle} >{item.title}</h5> }
                    <div className="card-footer">
                      <span className="text-muted">view:{item.views}</span>
                      <span className="text-muted"> like:{item.likes}</span>
                      <span className="text-muted"> {item.type}</span>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          )
        })}
          
      </div>

 <Pagination
        items={Post.length}
        currentPage={CurrentPage}
        pageSize={pageSize}
        onPageChange={handlePageChange}
      /> 

    </Layout>
  )
}

export default Index
