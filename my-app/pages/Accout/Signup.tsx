import React, { ReactElement, useState } from 'react';
import Layout from '../../component/Layout';
import { postUser } from '../../lib/post';
import { useRouter } from 'next/router';
import validator from 'validator';
  

interface Props {
    email: string,
    name: string,
    password: string
}
function Signup({ }: Props): ReactElement {
    const [email, setEmail] = useState<string>();
    const [name, setName] = useState<string>();
    const [password, setPassword] = useState<string>();


    const route = useRouter();

    const handleSend = async () => {
        if (!email || !name || !password) {
            alert("không được bỏ trống")
        }
        else if(!validator.isEmail(email)){
            alert("điền đúng định dạng Gmail đi")
        }
        else {
            try {
                await postUser({ email, name, password });
                route.push("/Accout/Login");
            } catch (error) {
                // console.log("Error", error.response.data.message);
            }
        }
    }
    return (
        <Layout>
            <div className="login-background">
                <div className="login-container">
                    <div className="login-content row">
                        <div className="col-12 text-login ">SignUp</div>
                        <div className="col-12 form-group login-input">
                            <label>Email:</label>
                            <input type="email" className="form-control" placeholder="Enter your Gmail:abc@gmail.com"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <div className="col-12 form-group login-input">
                            <label>Username:</label>
                            <input type="text" className="form-control" placeholder="Enter your username"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </div>


                        <div className="col-12 form-group login-input">
                            <label>PassWord:</label>
                            <input type="password" className="form-control" placeholder="Enter your PassWord"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <div className="col-12 ">
                            <button className=" btn-login"
                                onClick={handleSend}
                            >SignUp</button>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Signup

