import React, { ReactElement, useState } from 'react'
import Layout from '../../component/Layout';
import { LoginUser } from '../../lib/post';
import { useRouter } from 'next/router';

interface Props {
    email: string,
    password: string
}
function Login({ }: Props): ReactElement {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const router = useRouter();
    const handleLogin = async () => {
        if (!email || !password) {
            alert('điền đủ tài khoản, mật khẩu vào đê')
        } else {
            try {
                await LoginUser({ email, password });
                router.push("/")
            } catch (error) {
                // console.log(error.response.data.message);
alert("kiem tra lai tai khoan de")

            }
        }

    }
    const RouterDK = () => {
        router.push("/Accout/Signup")
    }


    return (
        <Layout>
            <div className="login-background">
                <div className="login-container">
                    <div className="login-content row">
                        <div className="col-12 text-login ">Login</div>
                        <div className="col-12 form-group login-input">
                            <label>Username:</label>
                            <input type="text" className="form-control" placeholder="abc@gmail.com"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>

                        <div className="col-12 form-group login-input">
                            <label>Password:</label>
                            <input type="password" className="form-control" placeholder="Enter your password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>


                        {/* <div className='col-12' style={{ color: 'red' }}>
                        {this.state.errMessage}
                    </div> */}
                        <div className="col-12 ">
                            <button className=" btn-login"
                                onClick={handleLogin}
                            >Login</button>
                        </div>
                        <div className="col-12">
                            <span className="forgot-password">Forgot your Password?</span>
                        </div>
                        <div>
                            <h6> Chưa có tài khoản thì đăng kí ở đây nhé </h6>
                            <button style={{ borderStyle: "dotted" }} onClick={RouterDK}>Đăng kí</button>
                        </div>

                    </div>
                </div>
            </div>
        </Layout>
    )
}


export default Login
